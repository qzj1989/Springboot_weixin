package com.sell;


import com.sell.model.OrderDetail;
import com.sell.model.OrderMaster;
import com.sell.model.ProductCategory;
import com.sell.model.ProductInfo;
import com.sell.model.vo.OrderMasterQueryModel;
import com.sell.model.vo.ProductInfoQueryModel;
import com.sell.service.Interface.OrderDetailService;
import com.sell.service.Interface.OrderMasterService;
import com.sell.service.Interface.ProductInfoService;
import com.sell.service.Interface.ProductService;
import com.sell.util.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SellApplicationTests {
    /*private final Logger logger=  LoggerFactory.getLogger(SellApplicationTests.class);*/
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductInfoService productInfoService;
    @Autowired
    private OrderMasterService orderMasterService;
    @Autowired
    private OrderDetailService orderDetailService;



    @Test
    public void  TestOrderDetailInsert(){

        OrderDetail orderDetail=new OrderDetail();

        orderDetail.setDetailId("1");
        orderDetail.setCreateTime(new Date());
        orderDetail.setOrderId("0001");
        orderDetail.setProductIcon("http://eeeee");
        orderDetail.setProductId("3333");
        orderDetail.setProductName("333名称");
        orderDetail.setProductPrice(new BigDecimal(0));
        orderDetail.setProductQuantity(11);
        orderDetail.setUpdateTime(new Date());
        Integer a=orderDetailService.Insert(orderDetail);
        System.out.println(a);

    }


 @Test
 public void TestQueryOrderDetailById(){

        OrderDetail orderDetail=orderDetailService.selectByPrimaryKey("1");

     System.out.println(orderDetail);
 }







    @Test
    public void TestOrderMasterInsert(){

        OrderMaster orderMaster=new OrderMaster();
  for (int i=0;i<8;i++){

      orderMaster.setOrderId(String.valueOf(i));
      orderMaster.setBuyerAddress("上海市"+i);
      orderMaster.setBuyerName("齐振江"+i);
      orderMaster.setBuyerOpenid("111111");
      orderMaster.setBuyerPhone("18336072796");
      orderMaster.setCreateTime(new Date());
      orderMaster.setOrderAmount(new BigDecimal(i));
      orderMaster.setPayStatus(i);
      orderMaster.setUpdateTime(new Date());
      Integer a= orderMasterService.insert(orderMaster);
      System.out.println(a);
  }
  }



@Test
    public  void testQueryOrderMaster(){
        Integer pageIndex = 1;
        Integer pageSize = 5;
        Integer total = orderMasterService.count();
        PageUtil pageUtil = new PageUtil(pageIndex, pageSize, total);

        OrderMasterQueryModel orderMasterQueryModel=new OrderMasterQueryModel();


        orderMasterQueryModel.setPageUtil(pageUtil);

      List<OrderMaster>  orderMasters=  orderMasterService.findBybuyerOpenidPage("111111",orderMasterQueryModel);


        System.out.println(orderMasters);



    }



    @Test
    public void contextLoads() {

   /*     List<ProductInfo> productInfos = productInfoService.findalll();*/


/*
        PageHelper.startPage(1,1);

     PageInfo  pageInfo=   new PageInfo(productCategories);*/
   /*     System.out.println(productInfos);*/
        Integer pageIndex = 1;
        Integer pageSize = 5;
        Integer total = productInfoService.count();
        PageUtil pageUtil = new PageUtil(pageIndex, pageSize, total);
        ProductInfoQueryModel  productInfoQueryModel=new ProductInfoQueryModel();
        productInfoQueryModel.setPageUtil(pageUtil);
        List<ProductInfo> productInfoList=productInfoService.findPage(productInfoQueryModel);

        System.out.println(productInfoList);

       /* ProductCategory productCategory = productService.findproductById(2);
        System.out.println(productCategory);*/


    }

    @Test
    public void TestConnectDatabase() {

/*	ProductCategory productCategory= productService.findproductById(1);
        System.out.println(productCategory);*/

        ProductCategory ProductCategory = new ProductCategory();

        ProductCategory.setCategoryName("我是测试3");

        ProductCategory.setCategoryType(2);

        ProductCategory.setCreateTime(new Date());

        ProductCategory.setUpdateTime(new Date());
        int a = productService.InsertProduct(ProductCategory);

        System.out.println(a);


    }


    @Test
    public void TestproductInfo() {

        for (int i = 0; i < 10; i++) {

            ProductInfo productInfo = new ProductInfo();
            productInfo.setProductId(String.valueOf(i));
            productInfo.setProductName("气" + i);
            productInfo.setCategoryType(i);
            productInfo.setCreateTime(new Date());
            productInfo.setProductDescription("eeeeeee" + i);
            productInfo.setProductIcon("图标" + i);
            productInfo.setProductPrice(new BigDecimal(11));
            productInfo.setProductStatus((byte) 0);
            productInfo.setProductStock(i);
            productInfo.setUpdateTime(new Date());

            productInfoService.Insert(productInfo);
        }


    }

    @Test
    public void PageproductInfo() {

      /*  ProductInfoQueryModel productInfoQueryModel = new ProductInfoQueryModel();



        List<ProductInfo> lists = productInfoService.findPage(productInfoQueryModel);
        System.out.println(lists);*/
    }


}
