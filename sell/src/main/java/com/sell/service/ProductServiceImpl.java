package com.sell.service;

import com.sell.Mapper.ProductCategoryMapper;
import com.sell.model.ProductCategory;
import com.sell.service.Interface.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author qizhenjiang
 * @create 2018-04-18 16:27
 * @Description:
 **/
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    @Override
    public ProductCategory findproductById(Integer id) {
        return productCategoryMapper.selectByPrimaryKey(id);
     }

    @Override
    public List<ProductCategory> findall() {
        return productCategoryMapper.find();
    }

    @Override
    public List<ProductCategory> findBycategoryTypein(List<Integer> categoryTypeList) {
        return productCategoryMapper.findBycategoryTypein(categoryTypeList);
    }

    @Override
    public int InsertProduct(ProductCategory productCategory) {

        return productCategoryMapper.insert(productCategory);
    }
}
