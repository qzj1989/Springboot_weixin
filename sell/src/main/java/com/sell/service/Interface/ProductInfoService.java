package com.sell.service.Interface;

import com.sell.model.ProductInfo;
import com.sell.model.vo.ProductInfoQueryModel;

import java.util.List;

/**
 * 功能描述:
 *
 * @author:
 * @date: 2018/4/19 20:40
 * @Description:商品模块
 */


public interface ProductInfoService {

    /**
     * 功能描述:
     *
     * @author: Administrator
     * @date: 2018/4/19 20:43
     * @Description:查询商品的总条数
     */

    Integer count();

    /**
     * 功能描述:
     *
     * @author: qi
     * @date: 2018/4/19 20:42
     * @Description:商品的分页
     */


    List<ProductInfo> findPage(ProductInfoQueryModel productInfoQueryModel);

    /**
     * 功能描述:
     *
     * @author: Administrator
     * @date: 2018/4/19 20:42
     * @Description:商品的增加
     */


    int Insert(ProductInfo productInfo);

    /**
     * 功能描述:
     *
     * @author: Administrator
     * @date: 2018/4/19 20:43
     * @Description:查询所有商品
     */


    List<ProductInfo> findalll();


    /**
     * 功能描述:
     *
     * @author: Administrator
     * @date: 2018/4/19 20:46
     * @Description:根据商品的id去查询商品
     */


    ProductInfo findByProductId(String ProductId);


}
