package com.sell.service.Interface;

import com.sell.model.ProductCategory;

import java.util.List;

/**
 * @author qizhenjiang
 * @create 2018-04-18 16:28
 * @Description:
 **/
public interface ProductService {
    /**
     * 功能描述:
     *
     * @author: qi
     * @date: 2018/4/18 16:29
     * @Description: 根据类目的id去查找类目
     */
    ProductCategory findproductById(Integer id);

    /**
     * 功能描述:
     *
     * @author: qi
     * @date: 2018/4/18 16:29
     * @Description: 查询所有的类目
     */

    List<ProductCategory> findall();

    /**
     * 功能描述:
     *
     * @author: qi
     * @date: 2018/4/18 16:29
     * @Description: 根据类目的type去查询线管的类目
     */

    List<ProductCategory> findBycategoryTypein(List<Integer> categoryTypeList);

    /**
     * 功能描述:
     *
     * @author: qi
     * @date: 2018/4/19 10:58
     * @Description:类目的新增
     */
    int InsertProduct(ProductCategory productCategory);







}
