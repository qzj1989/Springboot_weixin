package com.sell.service;

import com.sell.Mapper.ProductInfoMapper;
import com.sell.model.ProductInfo;
import com.sell.model.vo.ProductInfoQueryModel;
import com.sell.service.Interface.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 功能描述:
 *
 * @author: Administrator
 * @date: 2018/4/19 20:44
 * @Description:商品模块
 */

@Service
public class ProductInfoServiceImpl implements ProductInfoService {
    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Override
    public List<ProductInfo> findPage(ProductInfoQueryModel productInfoQueryModel) {
        return productInfoMapper.findPage(productInfoQueryModel);
    }

    @Override
    public int Insert(ProductInfo productInfo) {
        return productInfoMapper.insert(productInfo);
    }

    @Override
    public List<ProductInfo> findalll() {
        return productInfoMapper.find();
    }

    @Override
    public ProductInfo findByProductId(String ProductId) {
        return productInfoMapper.selectByPrimaryKey(ProductId);
    }

    @Override
    public Integer count() {
        return productInfoMapper.count();
    }


}
