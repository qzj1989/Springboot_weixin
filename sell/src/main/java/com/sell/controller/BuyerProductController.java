package com.sell.controller;

import com.sell.model.ProductCategory;
import com.sell.model.ProductInfo;
import com.sell.model.vo.ProductInfoVo;
import com.sell.model.vo.ProductVo;
import com.sell.model.vo.ResultVo;
import com.sell.service.Interface.ProductInfoService;
import com.sell.service.Interface.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * 功能描述: 
 *
 * @author: Administrator
 * @date: 2018/4/19 21:41
 * @Description:
 */


@RestController
@RequestMapping("/buyer/product")
public class BuyerProductController {
    /*商品*/
    @Autowired
    private ProductInfoService productInfoService;
    /*类目*/
    @Autowired
    private ProductService productService;

    /**
     *
     * 功能描述:
     *
     * @author: qi
     * @date: 2018/4/19 22:03
     * @Description:
     */
@RequestMapping("/list")
    public ResultVo list(){

/*查询所有商品*/
   List<ProductInfo> productInfos= productInfoService.findalll();

/*拿到所有商品的类目编号*/
List<Integer> categoryTypes=productInfos.stream().map(e -> e.getCategoryType()).collect(Collectors.toList());
/*根据所有商品类目的编号去取到所有类目的相关信息*/
List<ProductCategory>  productCategories=productService.findBycategoryTypein(categoryTypes);
List<ProductVo> productVos=new ArrayList<ProductVo>();
    
    for (ProductCategory category:productCategories
         ) {
        ProductVo productVo=new ProductVo();
        productVo.setCategoryType(category.getCategoryType());
        productVo.setCategoryName(category.getCategoryName());
        List<ProductInfoVo> productInfoVos=new ArrayList<ProductInfoVo>();
        for (ProductInfo productInfo:productInfos
             ) {
            if (productInfo.getCategoryType().equals(category.getCategoryId())){


                ProductInfoVo productInfoVo=new ProductInfoVo();

                BeanUtils.copyProperties(productInfo,productInfoVo);
                productInfoVos.add(productInfoVo);
            }
        }

        productVo.setProductInfoVoList(productInfoVos);
        productVos.add(productVo);
    }

    ResultVo resultVo=new ResultVo();
    resultVo.setCode(0);
    resultVo.setMsg("成功");
     resultVo.setData(productVos);
    return resultVo;
}



}
