package com.sell.Mapper;

import com.sell.model.SellerInfo;

public interface SellerInfoMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table seller_info
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table seller_info
     *
     * @mbg.generated
     */
    int insert(SellerInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table seller_info
     *
     * @mbg.generated
     */
    int insertSelective(SellerInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table seller_info
     *
     * @mbg.generated
     */
    SellerInfo selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table seller_info
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(SellerInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table seller_info
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(SellerInfo record);
}