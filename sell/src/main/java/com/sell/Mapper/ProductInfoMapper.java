package com.sell.Mapper;
import java.util.List;

import com.sell.model.ProductInfo;
import com.sell.model.vo.ProductInfoQueryModel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductInfoMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_info
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String productId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_info
     *
     * @mbg.generated
     */
    int insert(ProductInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_info
     *
     * @mbg.generated
     */
    int insertSelective(ProductInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_info
     *
     * @mbg.generated
     */
    ProductInfo selectByPrimaryKey(String productId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_info
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(ProductInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table product_info
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(ProductInfo record);

    /**
     * @return
     */
    List<ProductInfo> findPage(ProductInfoQueryModel productInfoQueryModel);



    List<ProductInfo> find();
    Integer count();


















}