package com.sell.model.vo;

import lombok.Data;

@Data
public class ResultVo<T> {
/*错误码*/
private  Integer code;
/*提示消息*/
private  String msg;
/*返回的具体内容*/
private  T data;

}
