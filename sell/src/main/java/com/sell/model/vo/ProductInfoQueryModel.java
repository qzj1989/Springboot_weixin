package com.sell.model.vo;

import com.sell.model.ProductInfo;
import com.sell.util.PageUtil;
import lombok.Data;
/**
 *
 * 功能描述:
 *
 * @author: qi
 * @date: 2018/4/19 16:49
 * @Description:分页model的组装
 */
@Data
public class ProductInfoQueryModel extends ProductInfo {

    private PageUtil  pageUtil;
}
